﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZavenDotNetInterview.App.Models
{
    public class Job
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public JobStatus Status { get; set; }
        public DateTime? DoAfter { get; set; }
        public virtual List<Log> Logs { get; set; }

        public Job() {}
        public Job(string name, DateTime? doAfter)
        {
            Id = Guid.NewGuid();
            Status = JobStatus.New;
            Created = DateTime.UtcNow;
            Name = name;
            DoAfter = doAfter;
        }
    }

    public enum JobStatus
    {
        Failed = -1,
        New = 0,
        InProgress = 1,
        Done = 2
    }
}