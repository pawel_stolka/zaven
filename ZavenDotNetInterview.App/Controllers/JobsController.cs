﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZavenDotNetInterview.App.Models;
using ZavenDotNetInterview.App.Models.Context;
using ZavenDotNetInterview.App.Repositories;
using ZavenDotNetInterview.App.Services;

namespace ZavenDotNetInterview.App.Controllers
{
    public class JobsController : Controller
    {
        private readonly IJobProcessorService _jobProcessorService;
        private readonly IJobsRepository _jobsRepository;
        private const string INDEX = "Index";

        public JobsController(IJobsRepository jobsRepository, IJobProcessorService jobProcessorService)
        {
            _jobsRepository = jobsRepository;
            _jobProcessorService = jobProcessorService;
        }

        // GET: Tasks
        public ActionResult Index()
        {
            List<Job> jobs = _jobsRepository.GetAllJobs()
                .OrderByDescending(x => x.Created)
                .ToList();
            
            return View(jobs);
        }

        // POST: Tasks/Process
        [HttpGet]
        public ActionResult Process()
        {
            _jobProcessorService.ProcessJobs();

            return RedirectToAction(INDEX);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tasks/Create
        [HttpPost]
        public ActionResult Create(string name, DateTime doAfter)
        {
            try
            {
                //TODO: make Fabric_______________!!!!!!!!!!!!!!!!!
                Job newJob = new Job(name, doAfter);

                _jobsRepository.AddJob(newJob);

                return RedirectToAction(INDEX);
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(Guid jobId)
        {
            return View();
        }
    }
}
